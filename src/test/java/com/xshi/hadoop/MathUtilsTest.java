package com.xshi.hadoop;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MathUtilsTest
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MathUtilsTest(String testName)
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MathUtilsTest.class );
    }

    /**
     * Test the cosine similarity calculator.
     */
    public void testCosineSimilarity()
    {
        double a [] = {2/3d, 1/3d, 0};
        double b [] = {0, 1/4d, 3/4d};

        double cs = MathUtils.calculateCosineSimilarity(a, b);
        System.out.println(cs);
        assertTrue(cs > 0.1414d);
        assertTrue(cs < 0.1415d);
    }
}
