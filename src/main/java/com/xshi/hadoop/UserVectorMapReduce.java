package com.xshi.hadoop;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.*;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public class UserVectorMapReduce {
    public static class UserVectorMapper extends MapReduceBase implements Mapper<Text, IntWritable, Text, Text> {

        private Text user = new Text();
        private Text vector = new Text();

        // output format: viewer   item#1=count#1
        @Override
        public void map(Text userItem, IntWritable count, OutputCollector<Text, Text> textTextOutputCollector, Reporter reporter) throws IOException {
            String [] strs = userItem.toString().split(",");
            if (strs == null || strs.length != 2) {
                System.err.println("Incorrect userItem key format: " + userItem.toString());
                return;
            }

            String userStr = strs[0];
            String itemStr = strs[1];

            if (userStr == null || itemStr == null) {
                System.err.println("Missing user or item key: " + userItem.toString());
                return;
            }

            user.set(userStr);
            vector.set(itemStr + "=" + count.toString());

            textTextOutputCollector.collect(user, vector);
        }
    }

    public static class UserVectorReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        private Text itemPair = new Text();
        private Text countPair = new Text();

        // output format: item#1,item#2   count#1,count#2
        @Override
        public void reduce(Text user, Iterator<Text> values, OutputCollector<Text, Text> textTextOutputCollector, Reporter reporter) throws IOException {
            Map<String, Integer> itemCounts = new HashMap<String, Integer>();
            while(values.hasNext()) {
                Text itemCount = values.next();
                String [] strs = itemCount.toString().split("=");
                if (strs == null || strs.length != 2) {
                    System.err.println("Incorrect itemCount format: " + itemCount.toString());
                    continue;
                }
                try {
                    itemCounts.put(strs[0], Integer.parseInt(strs[1]));
                } catch(NumberFormatException nfe) {
                    System.err.println("Invalid itemCount format: " + itemCount.toString());
                    continue;
                }
            }

            Set<String> itemSet = itemCounts.keySet();
            String [] items = itemSet.toArray(new String[itemSet.size()]);

            for(int i=0; i<items.length-1; i++) {
                for(int j=i+1; j<items.length; j++) {
                    itemPair.set(items[i] + "," + items[j]);
                    countPair.set(itemCounts.get(items[i]) + "," + itemCounts.get(items[j]));
                    textTextOutputCollector.collect(itemPair, countPair);
                }
            }
        }
    }

}
