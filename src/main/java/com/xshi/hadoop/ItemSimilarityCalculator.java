package com.xshi.hadoop;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */

public class ItemSimilarityCalculator extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int res = ToolRunner.run(new Configuration(true), new ItemSimilarityCalculator(), args);
        System.exit(res);
    }

    @Override
    public int run(String[] args) throws Exception {

        Path inputPath = new Path(args[0]);
        Path outputDir = new Path(args[1]);

        // Create configuration
        Configuration conf = getConf();
        conf.set("mapred.child.java.opts", "-Xms256m -Xmx2g -XX:+UseSerialGC");
        conf.set("mapred.job.map.memory.mb", "512");
        conf.set("mapred.job.reduce.memory.mb", "2048");

        // 1. Create and run user-item count MapReduce job
        Path userItemCountDir = new Path("/tmp/affinity/userItemCounts/");
        JobConf userItemCountMapReduce = createJob(conf,
                UserItemCountMapReduce.UserItemCountMapper.class, UserItemCountMapReduce.UserItemCountReducer.class, 10,
                new Path[] {inputPath}, userItemCountDir,
                TextInputFormat.class, SequenceFileOutputFormat.class,
                Text.class, IntWritable.class,
                Text.class, IntWritable.class,
                "User-Item Count MapReduce");
        JobClient.runJob(userItemCountMapReduce);

        // 2.Calculate sum and square sum of each item
        Path itemVectorSumsDir = new Path("/tmp/affinity/itemVectorSums/");
        JobConf itemVectorSumsMapReduce = createJob(conf,
                ItemVectorMapReduce.ItemVectorMapper.class, ItemVectorMapReduce.ItemVectorReducer.class, 10,
                new Path[] {userItemCountDir}, itemVectorSumsDir,
                SequenceFileInputFormat.class, TextOutputFormat.class,
                Text.class, Text.class,
                Text.class, Text.class,
                "Item Vector Sums and Square Sums MapReduce");
        JobClient.runJob(itemVectorSumsMapReduce);

        // 3. Create and run user vector MapReduce job
        Path userVectorDir = new Path("/tmp/affinity/userVectors/");
        JobConf userVectorMapReduce = createJob(conf,
                UserVectorMapReduce.UserVectorMapper.class, UserVectorMapReduce.UserVectorReducer.class, 10,
                new Path[] {userItemCountDir}, userVectorDir,
                SequenceFileInputFormat.class, SequenceFileOutputFormat.class,
                Text.class, Text.class,
                Text.class, Text.class,
                "User Vector MapReduce");
        JobClient.runJob(userVectorMapReduce);

        // 4. Create and run item vector and cosine similarity calulator MapReduce job
        JobConf itemVectorMapReduce = createJob(conf,
                null, CosineSimilarityMapReduce.CosineSimilarityReducer.class, 4,
                new Path[] {userVectorDir}, outputDir,
                SequenceFileInputFormat.class, TextOutputFormat.class,
                Text.class, Text.class,
                Text.class, DoubleWritable.class,
                "Cosine Similarity MapReduce");
        JobClient.runJob(itemVectorMapReduce);

        return 0;
    }


    protected JobConf createJob(Configuration conf,
                                Class mapperClass, Class reducerClass, int reducerCount,
                                Path [] inputPaths, Path outputDir,
                                Class inputFormatClass, Class outputFormatClass,
                                Class mapOutputKeyClass, Class mapOutputValueClass,
                                Class outputKeyClass, Class outputValueClass,
                                String name) throws IOException {
        // Create job
        JobConf job = new JobConf(conf);
        job.setJobName(name);

        // Setup MapReduce
        if (mapperClass != null)
            job.setMapperClass(mapperClass);
        if (reducerClass != null)
            job.setReducerClass(reducerClass);
        if (reducerCount > 0)
            job.setNumReduceTasks(reducerCount);

        // Specify key / value
        job.setMapOutputKeyClass(mapOutputKeyClass);
        job.setMapOutputValueClass(mapOutputValueClass);
        job.setOutputKeyClass(outputKeyClass);
        job.setOutputValueClass(outputValueClass);

        // Input
        FileInputFormat.setInputPaths(job, inputPaths);
        job.setInputFormat(inputFormatClass);

        // Delete output if exists
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);
        // A hack for standalone cluster
        hdfs = FileSystem.getLocal(conf);
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Output
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormat(outputFormatClass);

        return job;
    }


}