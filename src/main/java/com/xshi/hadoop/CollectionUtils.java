package com.xshi.hadoop;

import java.util.Iterator;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public class CollectionUtils {

    public static <T> int sizeOf(Iterator<T> iterator) {
        int size = 0;
        while(iterator.hasNext()) {
            size++;
        }
        return size;
    }
}
