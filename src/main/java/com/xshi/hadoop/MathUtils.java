package com.xshi.hadoop;

import java.util.List;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public final class MathUtils {

    public static final double calculateCosineSimilarity(double [] a, double [] b) {
        if (a == null || b == null || a.length != b.length)
            throw new IllegalArgumentException("Invalid input arrays");

        double dotSum = dotSum(a, b);
        double aSquareSum = squareSum(a);
        double bSquareSum = squareSum(b);

        if (dotSum == 0d || aSquareSum <= 0d || bSquareSum <= 0d)
            return 0d;
        else
            return dotSum / (Math.sqrt(aSquareSum * bSquareSum));
    }

    public static final double dotSum(double [] a, double [] b) {
        if (a == null || b == null || a.length != b.length)
            throw new IllegalArgumentException("Invalid input arrays");

        if (a.length == 0)
            return 0d;

        double dotSum = 0d;
        for(int i=0; i<a.length; i++) {
            dotSum += a[i] * b[i];
        }
        return dotSum;
    }

    public static final double squareSum(double [] array) {
        if (array == null)
            throw new IllegalArgumentException("Invalid input arrays");

        if (array.length == 0)
            return 0d;

        double squareSum = 0d;
        for(int i=0; i<array.length; i++) {
            squareSum += Math.pow(array[i], 2d);
        }

        return squareSum;
    }


    public static final int sum(int [] array) {
        int sum = 0;
        for(int i=0; i<array.length; i++) {
            sum+=array[i];
        }
        return sum;
    }

    public static final int sum(Integer [] array) {
        int sum = 0;
        for(int i=0; i<array.length; i++) {
            sum+=array[i];
        }
        return sum;
    }

    public static final double sum(double [] array) {
        double sum = 0;
        for(int i=0; i<array.length; i++) {
            sum+=array[i];
        }
        return sum;
    }

    public static final int sum(List<Integer> list) {
        int sum = 0;
        for(Integer i : list) {
            if (i != null)
                sum+=i.intValue();
        }
        return sum;
    }

    public static final void normalize(double [] array) {
        final double sum = sum(array);
        for(int i=0; i<array.length; i++) {
            array[i] /= sum;
        }
    }

    public static final double [] normalize(int [] array) {
        final double sum = sum(array);
        double [] normalized = new double[array.length];
        for(int i=0; i<normalized.length; i++) {
            normalized[i] = array[i] / sum;
        }
        return normalized;
    }

    public static final double [] normalize(Integer [] array) {
        final double sum = sum(array);
        double [] normalized = new double[array.length];
        for(int i=0; i<normalized.length; i++) {
            normalized[i] = array[i] / sum;
        }
        return normalized;
    }

    public static final double [] normalize(List<Integer> list) {
        if (list == null || list.size() == 0)
            throw new IllegalArgumentException("Input list can not be null or empty");

        Integer [] ints = list.toArray(new Integer[list.size()]);
        return normalize(ints);
    }
}
