package com.xshi.hadoop;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.Iterator;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public class UserItemCountMapReduce {

    public static class UserItemCountMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, IntWritable> {

        private final IntWritable ONE = new IntWritable(1);
        private Text userItem = new Text();

        // output format: viewer,item   1
        @Override
        public void map(LongWritable key, Text value, OutputCollector<Text, IntWritable> textIntWritableOutputCollector, Reporter reporter) throws IOException {
            String [] strs = value.toString().split("\t");
            if (strs == null || strs.length != 2) {
                System.err.println("Incorrect input value format: " + value.toString());
                return;
            }

            userItem.set(strs[0] + "," + strs[1]);
            textIntWritableOutputCollector.collect(userItem, ONE);
        }
    }


    public static class UserItemCountReducer extends MapReduceBase implements Reducer<Text, IntWritable, Text, IntWritable> {

        // output format: viewer,item  sum(e.g. 5)
        @Override
        public void reduce(Text userItem, Iterator<IntWritable> values, OutputCollector<Text, IntWritable> textIntWritableOutputCollector, Reporter reporter) throws IOException {
            int sum = 0;
            while(values.hasNext()) {
                IntWritable value = values.next();
                sum += value.get();
            }
            textIntWritableOutputCollector.collect(userItem, new IntWritable(sum));
        }
    }
}
