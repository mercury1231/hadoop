package com.xshi.hadoop;

import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public class CosineSimilarityMapReduce {

    public static class CosineSimilarityReducer extends MapReduceBase implements Reducer<Text, Text, Text, DoubleWritable> {

        private static final DoubleWritable ZERO = new DoubleWritable(0d);

        private Text itemPairKey = new Text();
        private Text countPair = new Text();

        private Map<String, Sums> itemSumMap = null;


        @Override
        public void configure(JobConf job) {
            super.configure(job);

            itemSumMap = new HashMap<String, Sums>();
            String line = null;
            try{
                Path path = new Path("/tmp/affinity/itemVectorSums/");
                FileSystem fs = FileSystem.get(job);
                FileStatus[] status = fs.listStatus(path);
                for (int i=0;i<status.length;i++){
                    BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));

                    line=br.readLine();
                    while (line != null){
                        String [] strs = line.split("\t");
                        String [] sums = strs[1].split(",");

                        itemSumMap.put(strs[0], Sums.of(Integer.parseInt(sums[0]), Double.parseDouble(sums[1])));

                        line=br.readLine();
                    }
                }
                //System.out.println("Loaded " + itemSumMap.size() + " item sums into map");
                if (itemSumMap.size() > 66170) {
                    String errMsg = "For security reasons, this application is programmed to load no more than 66170 items.";
                    System.err.println(errMsg);
                    throw new RuntimeException(errMsg);
                }

            } catch(Exception e){
                System.err.println("Error loading item sums: " + line);
                throw new RuntimeException(e);
            }
        }

        // input format: item#1,item#2   count#1,count#2
        // output format: (item#1,item2)   cosine_similarity
        @Override
        public void reduce(Text itemPair, Iterator<Text> countPairValues, OutputCollector<Text, DoubleWritable> textDoubleWritableOutputCollector, Reporter reporter) throws IOException {
            String [] strs = itemPair.toString().split(",");
            if (strs == null || strs.length != 2) {
                System.err.println("Incorrect itemPair format: " + itemPair.toString());
                return;
            }
            String item1 = strs[0];
            String item2 = strs[1];

            itemPairKey.set("(" + item1 + "," + item2 + ")");


            List<Integer> list1 = new ArrayList<Integer>();
            List<Integer> list2 = new ArrayList<Integer>();

            while(countPairValues.hasNext()) {
                Text countPair = countPairValues.next();
                strs = countPair.toString().split(",");
                if (strs == null || strs.length != 2) {
                    System.err.println("Incorrect countPair format: " + countPair.toString());
                    continue;
                }
                try {
                    list1.add(Integer.parseInt(strs[0]));
                    list2.add(Integer.parseInt(strs[1]));
                } catch(NumberFormatException nfe) {
                    System.err.println("Invalid countPair format: " + countPair.toString());
                    continue;
                }
            }

            if (list1.size() != list2.size()) {
                System.err.println("Somehow sizes of the item vectors do not match: " + list1.size() + " vs. " + list2.size());
                return;
            }

            final int size = list1.size();
            if (size == 0)
                textDoubleWritableOutputCollector.collect(itemPair, ZERO);


            Sums sums1 = itemSumMap.get(item1);
            Sums sums2 = itemSumMap.get(item2);
            if (sums1 == null || sums2 == null) {
                System.err.println("Unable to locate sums for item(s): " + item1 + ", " + item2);
            }

            double [] counts1 = normalize(list1, sums1.getSum());
            double [] counts2 = normalize(list2, sums2.getSum());

            double dotSum = MathUtils.dotSum(counts1, counts2);

            double cs = dotSum / Math.sqrt(sums1.getSquareNormalizedSum() * sums2.getSquareNormalizedSum());

            textDoubleWritableOutputCollector.collect(itemPairKey, new DoubleWritable(cs));
        }

        protected double [] normalize(List<Integer> counts, final int sum) {
            double [] array = new double[counts.size()];
            for(int i=0; i<array.length; i++) {
                array[i] = (double)(counts.get(i)) / sum;
            }
            return array;
        }


        static final class Sums {
            private int sum;
            private double squareNormalizedSum;

            protected Sums(int sum, double squareNormalizedSum) {
                this.sum = sum;
                this.squareNormalizedSum = squareNormalizedSum;
            }

            private int getSum() {
                return sum;
            }

            private void setSum(int sum) {
                this.sum = sum;
            }

            private double getSquareNormalizedSum() {
                return squareNormalizedSum;
            }

            private void setSquareNormalizedSum(double squareNormalizedSum) {
                this.squareNormalizedSum = squareNormalizedSum;
            }

            public static final Sums of(int sum, double squareNormalizedSum) {
                return new Sums(sum, squareNormalizedSum);
            }
        }
    }

}


