package com.xshi.hadoop;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/*************************************************************************
 *
 *  2014 Copyright Protected @ Xiaodong Shi
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of Xiaodong Shi,
 * if any.  The intellectual and technical concepts contained
 * herein are proprietary to Xiaodong Shi and may be covered
 * by U.S. and Foreign Patents, patents in process, and are
 * protected by trade secret or copyright law.
 * Commercial use and dissemination of this information or
 * reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Xiaodong Shi.
 *
 */
public class ItemVectorMapReduce {

    public static class ItemVectorMapper extends MapReduceBase implements Mapper<Text, IntWritable, Text, Text> {
        private Text item = new Text();
        private Text userCount = new Text();

        // input format: viewer,item  count
        // output format: item  viewer#1=count#1
        @Override
        public void map(Text userItem, IntWritable count, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
            String [] strs = userItem.toString().split(",");
            if (strs == null || strs.length != 2) {
                System.err.println("Incorrect userItem key format: " + userItem.toString());
                return;
            }

            String userStr = strs[0];
            String itemStr = strs[1];

            if (userStr == null || itemStr == null) {
                System.err.println("Missing user or item key: " + userItem.toString());
                return;
            }

            item.set(itemStr);
            userCount.set(userStr + "=" + count.toString());
            outputCollector.collect(item, userCount);
        }
    }

    public static class ItemVectorReducer extends MapReduceBase implements Reducer<Text, Text, Text, Text> {

        private static final DoubleWritable ZERO = new DoubleWritable(0d);

        private Text sums = new Text();

        // input format: item  viewer#1:count#1
        // output format: item  sum,square_normalized_sum
        @Override
        public void reduce(Text item, Iterator<Text> userCounts, OutputCollector<Text, Text> outputCollector, Reporter reporter) throws IOException {
            List<Integer> counts = new ArrayList<Integer>();

            while(userCounts.hasNext()) {
                Text userCount = userCounts.next();
                String [] strs = userCount.toString().split("=");
                if (strs == null || strs.length != 2) {
                    System.err.println("Incorrect userCount format: " + userCount.toString());
                    continue;
                }
                try {
                    counts.add(Integer.parseInt(strs[1]));
                } catch(NumberFormatException nfe) {
                    System.err.println("Invalid userCount format: " + userCount.toString());
                    continue;
                }
            }

            final int size = counts.size();
            if (size == 0) {
                System.err.println("No counts available for item " + item.toString());
                return;
            }

            int sum = MathUtils.sum(counts);
            double [] normCounts = MathUtils.normalize(counts);
            double squareSum = MathUtils.squareSum(normCounts);

            sums.set(sum + "," + squareSum);

            outputCollector.collect(item, sums);
        }
    }
}
