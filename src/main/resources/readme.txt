INSTRUCTIONS ON HOW TO RUN THIS:

1. Download latest Apache Hadoop distribution (I'm using 2.5.1):
http://www.apache.org/dyn/closer.cgi/hadoop/common/

2. Unwrap the .tar.gz file to your destination folder.
tar -xvzf hadoop-2.5.1.tar.gz

3. Assume the hadoop installation folder is $HADOOP_DIR. Also set the JAVA_HOME if not set. Path could vary depending on platform.
export $JAVA_HOME=/usr/java/default

4. Verify if hadoop command works.
$HADOOP_DIR/bin/hadoop fs -ls /

5. Create the working folder for affinity calculator.
$HADOOP_DIR/bin/hadoop fs -mkdir /tmp/affinity/

6. Upload input data file to HDFS.
$HADOOP_DIR/bin/hadoop fs -mkdir /tmp/affinity/input/
$HADOOP_DIR/bin/hadoop fs -put /path/to/input/file /tmp/affinity/input/

7. Run the affinity calculator job.
$HADOOP_DIR/bin/hadoop jar /path/to/jar/file com.xshi.hadoop.ItemSimilarityCalculator /tmp/affinity/input/ /tmp/affinity/output/

8. Export the results into a single file
$HADOOP_DIR/bin/hadoop fs -text /tmp/affinity/output/*  > /tmp/cosineSimilarity.txt
